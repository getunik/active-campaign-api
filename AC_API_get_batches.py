#############################################################################
"""
TO DOs:
+ Error logging => done
+ possibly missing page 0 => NO NEED, PAGE 0 IS COPY OF 1
+ Huston, we are missing data.
+ how to end the while loop? => get highest client id and end when ID = max_id
+ duplicates? asyncio.gather(*coros_or_futures, loop=None, return_exceptions=False)
    => use sort & sort_direction to avoid duplication error
+ batch_size should take less than 5 min to execute => by removing fields we are much faster
- check if we need API parameter full to be 1 or 0
"""
#############################################################################

# check the github for further info https://github.com/bennylope/activecampaign
import json
import requests
import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath('.'))) # to make imports relative to project root work
from dotenv import load_dotenv
import logging
from pathlib import Path
from urllib.request import urlopen, Request
import aiohttp
from time import time, sleep
import datetime
import asyncio
import pickle

# section to handel environmental variables
load_dotenv('.env')
token = os.getenv('AC_TOKEN')
url = os.getenv('AC_URL')
workingData = os.getenv('AC_DATAWORKING')

Path(workingData).mkdir(parents=True, exist_ok=True)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
# create a file handler
handler = logging.FileHandler(os.path.join(workingData, 'log_info.txt'))
handler.setLevel(logging.INFO)
# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(handler)
logger.info(f'See the log.info file')
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

batchsize = 200

# makes the API call to Active Campaign        
# gets the ac_json per page and sets up the aiohttp.ClientSession() as session
async def async_get_ac(page):
    async with aiohttp.ClientSession() as session:
        params = [('sort', 'id'),('sort_direction','ASC'), ("api_action", 'contact_list'), ("api_key", token), ("api_output", "json"), ("ids", 'all'), ("full", 1), ("page", page)] # full = 1 is required to get all activity information etc.
        async with session.get(url, headers={'Accept':'application/json'}, params=params) as resp:
            assert resp.status == 200
            data = await resp.read()
            response = json.loads(data)
            if isinstance(response, dict):
                if response['result_message'] == 'Success: Something is returned':
                    del response['result_code']
                    del response['result_message']
                    del response['result_output']
                    #we need to delete the column fields as too much unneccessary data is shown
                    # for client, values in response.items():
                    #     del response[client]['fields']
                    return response


# accumulates data to a batch and saves the output as pickle
async def get_batch(batch_start, batch_end, batch):
    finished_json_list = []
    tasks = [async_get_ac(i) for i in range(batch_start, batch_end)]
    # asyncio.wait should only return json response when completely downloaded
    finished, unfinished = await asyncio.wait(tasks, return_when=asyncio.ALL_COMPLETED)
    filename = f'batch_{batch}.pickle'
    # logger.info('finished tasks: ' + str(len(finished)) + ', unfinished tasks: ' + str(len(unfinished)))
    for task in finished:
        response = task.result()
        finished_json_list.append(response)      
         
    with open(os.path.join(workingData, filename), 'wb') as f:
        pickle.dump(finished_json_list, f)
    return finished_json_list

#  main script can run until no more data is returned
async def main(batch_size = batchsize, batch_start = 1, batch = 1):
    batch_end = batch_start + batch_size 
    # check how much data there is to collect
    logger.info('Assement of maximum id value')
    params_start = [("api_action", 'contact_list'), ("api_key", token), ("api_output", "json"), ("ids", 'all'),("full", 1),("page", 1),('sort', 'id'),('sort_direction','DESC')]
    response_start = requests.request('GET',url,params=params_start).json()
    current_id = 0
    max_id = 0
    del response_start['result_code']
    del response_start['result_message']
    del response_start['result_output']
    for client, values in response_start.items(): 
        if max_id < int(response_start[client]['id']):
            max_id = int(response_start[client]['id'])
    logger.info('max_id: ' + str(max_id))
    
    # Loop that gets data until we have reached max_id
    while current_id < max_id:
        finished_json_list = []
        # now we try and see if the resulting list_ac_json contains anything
        # if we did not receive an answer than we try again with more time before timeout
        # if both attemps are unsuccessful we save it to dictionary not_successful
        try:
            logger.info('start batch nr: ' + str(batch) + ', batch_start: ' + str(batch_start) + ', batch_end: ' + str(batch_end) )
            finished_json_list = await get_batch(batch_start = batch_start,batch_end = batch_end, batch = batch)
            current_id = int(finished_json_list[-1]['19']['id'])
            try:
                logger.info('Checking for max_id batch nr: ' + str(batch) + ' current_id: ' + str(current_id))
                for ac_json in finished_json_list:
                    if isinstance(ac_json, dict):
                        for contact, values in ac_json.items():
                            if int(ac_json[contact]['id']) == max_id:
                                current_id = int(ac_json[contact]['id'])
                                logger.info('ended successful, found max_id: ' + str(current_id) + ', in batch nr: ' + str(batch) + ', list_element: ' + str(contact))
                                break

            except Exception as e:
                logger.error(e)
                logger.warning('ended unsuccessful current_id: ' + str(current_id) + ', max_id: ' + str(max_id))
                break

            batch += 1
            batch_start += batch_size
            batch_end += batch_size
        except Exception as e:
            logger.error(e)
            logger.info('1. exception batch nr: ' + str(batch) + ' current_id: ' + str(current_id))
            # try:
            #     logger.info('1. retry batch nr: ' + str(batch) + ', batch_start: ' + str(batch_start) + ', batch_end: ' + str(batch_end))
            #     finished_json_list = await get_batch(batch_start = batch_start, batch_end = batch_end, batch = batch)       
            #     current_id = int(finished_json_list[-1]['19']['id'])
            #     logger.info('1. retry success batch nr: ' + str(batch) + ', batch_start: ' + str(batch_start) + ', batch_end: ' + str(batch_end))
            #     # as our code has failed twice lets see if this is the last batch, where we have the max_id we are looking for
            # except Exception as e:
            #     logger.error(e)
            #     logger.info('2. exception batch nr: ' + str(batch) + ' current_id: ' + str(current_id))



 #  creates a loop required for asyncronious calls
if __name__ == '__main__':
    ts = time()
    #Create the asyncio event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    finally:
        # Shutdown the loop even if there is an exception
        loop.close()
    logger.info('Took %s seconds to complete', time() - ts)

