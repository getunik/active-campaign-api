# README #

This repository aims to access all behavioural data from Active Campaign (AC), which cannot easily be retrieved from the Website. 


### What is this repository for? ###

* AC data concerning clickrate, obenrate and unsubscriberate etc.
* AC ECommerce Data for transactions etc.

### How do I get set up? ###

* Download repository
* Make sure credentials are set correctly in .env file
* install virtualenv using requirements.txt
* make sure the folder structure is established


### Contribution guidelines ###

* Both AC API v1 and v3 are used.
* https://developers.activecampaign.com/reference
* https://www.activecampaign.com/api/overview.php

### Who do I talk to? ###

* Daniel Barco