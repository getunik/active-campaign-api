import json
import pandas as pd
import requests
from pandas.io.json import json_normalize
#from activecampaign.client import ActiveCampaignClient
import os
from os import listdir
from os.path import isfile, join
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath('.'))) # to make imports relative to project root work
from pprint import pprint
from dotenv import load_dotenv
from multiprocessing import Process, Pool
import threading
import logging
from pathlib import Path
from urllib.request import urlopen, Request
import aiohttp
from time import time, sleep
import datetime
import asyncio
import pickle
import csv


load_dotenv('.env')
workingData = os.getenv('AC_DATAWORKING')
#print(workingData)


# Logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
# create a file handler
handler = logging.FileHandler(os.path.join(workingData, 'log_info.txt'))
handler.setLevel(logging.INFO)
# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(handler)
logger.info(f'See the log.info file')
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

#df = 0
counter_size = 8

def unpickle_all(directory,counter=0,multibatch=0):
    logger.info('Unpickle started')
    df = pd.DataFrame()
    list_ac_json = []
    counter = 0
    count_pickles = 0
    total_count = 0
    multibatch = 0
    for f in listdir(directory):
        filename, file_extension = os.path.splitext(f)
        if isfile(join(directory, f)) and file_extension == ".pickle":
            count_pickles += 1
    logger.info('total pickled files: ' + str(count_pickles))
    for f in listdir(directory):
        filename, file_extension = os.path.splitext(f)
        if isfile(join(directory, f)) and file_extension == ".pickle":
            counter += 1
            total_count += 1
            logger.info(str(f) + ' number ' + str(counter))
            with open(os.path.join(directory, f),"rb") as f2:
                list_ac_json = pickle.load(f2)
            for element in list_ac_json:
                try:
                    df = df.append(pd.DataFrame.from_dict(element).T,ignore_index=True,sort=False)
                except Exception as e:
                    logger.error(str(e) + ' batch nr: ' + str(f) + ', element:' + str(element))
                    continue
    
            if counter >= counter_size or total_count >= count_pickles:
                multibatch += 1
                filename = f'multibatch_{multibatch}.pickle'
                Path(os.path.join(os.path.join(workingData, 'multibatch'))).mkdir(parents=True, exist_ok=True)
                with open(os.path.join(os.path.join(workingData, 'multibatch'),filename), 'wb') as f:
                    pickle.dump(df, f)
                logger.info('Multibatch saved: ' +  str(multibatch) + ' ,length multibatch: ' + str(len(df)))
                df = 0
                df = pd.DataFrame()
                counter = 0

        
    #return df

def unpickle_multibatch(directory):
    df = pd.DataFrame()
    logger.info('unpickle_multibatch was started')
    for f in listdir(directory):
        filename, file_extension = os.path.splitext(f)
        if isfile(join(directory, f)) and file_extension == ".pickle" and not filename == 'multibatch_all':
            logger.info(str(f) + ' number ')
            with open(os.path.join(directory, f),"rb") as f2:
                df_new = pickle.load(f2)
                df = df.append(df_new,ignore_index=True,sort=False)
                logger.info('Multibatch all: ' +  str(f) + ' ,length multibatch: ' + str(len(df)))
    
    with open(os.path.join(os.path.join(workingData, 'multibatch'), 'multibatch_all.pickle'), 'wb') as f:
        pickle.dump(df, f)
    logger.info('multibatch_all.pickle was saved')
    return df


multibatch_directory = os.path.join(workingData, 'multibatch')
print(multibatch_directory)
unpickle_all(workingData)        
data = unpickle_multibatch(multibatch_directory)    
print(data)             
print(data['email'].value_counts())
print(len(data))
# print(len(data['email'].isna()))
x = datetime.datetime.now()
data.to_csv(os.path.join(workingData, 'AC_Contacts.csv'), sep='\t', encoding='utf-8')
logger.info('saved as CSV ' + 'AC_Contacts.csv')