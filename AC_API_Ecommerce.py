#############################################################################
"""
TO DOs:
+ Error logging => done
+ possibly missing page 0 => NO NEED, PAGE 0 IS COPY OF 1
+ Huston, we are missing data.
+ how to end the while loop? => get highest client id and end when ID = max_id
+ duplicates? asyncio.gather(*coros_or_futures, loop=None, return_exceptions=False)
    => use sort & sort_direction to avoid duplication error
+ batch_size should take less than 5 min to execute
- check if we need API parameter full to be 1 or 0
----
- make while loop functional
- 

"""
#############################################################################

# check the github for further info https://github.com/bennylope/activecampaign
import json
import pandas as pd
import requests
from pandas.io.json import json_normalize
#from activecampaign.client import ActiveCampaignClient
import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath('.'))) # to make imports relative to project root work
from pprint import pprint
from dotenv import load_dotenv
from multiprocessing import Process, Pool
import threading
import logging
from pathlib import Path
from urllib.request import urlopen, Request
import aiohttp
from time import time, sleep
import datetime
import asyncio
import pickle
import csv

# section to handel environmental variables
load_dotenv('.env')
token = os.getenv('AC_TOKEN')
url = os.getenv('AC_URLv3')
workingData = os.getenv('AC_DATAWORKING')
#print(workingData)

Path(workingData).mkdir(parents=True, exist_ok=True)
df = pd.DataFrame()
batchsize = 200

# logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
# create a file handler
print(workingData) 
handler = logging.FileHandler(os.path.join(workingData, 'log_info.txt'))
handler.setLevel(logging.INFO)
# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(handler)
logger.info(f'See the log.info file')
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# makes the APIv3 call to Active Campaign        
# gets the ac_json per page and sets up the aiohttp.ClientSession() as session
async def async_get_ac(page):
    async with aiohttp.ClientSession() as session:
        params = [("api_action", 'contact_list'), ("api_key", token), ("api_output", "json")]
        data = [('limit', 100), ('offset', (page-1)*100), ('orders[id]','ASC'), ('filters[state]',1)] 
        params += data
        async with session.get(url,params=params) as resp:
            return await resp.json()

# accumulates data to a batch and saves the output as pickle
# possible missing data if data is updated during async operation (we cannot sort by time)
async def get_batch(batch_start, batch_end, batch):
    finished_json_list = []
    tasks = [async_get_ac(i) for i in range(batch_start, batch_end)]
    # asyncio.wait should only return json response when completely downloaded
    finished, unfinished = await asyncio.wait(tasks, return_when=asyncio.ALL_COMPLETED)
    filename = f'batch_{batch}.pickle'
    try:
        #print(finished[0])
        for task in finished:
            response = task.result()
            #print(type(response))
            #print(response['ecomOrders'])
            # so that we can perform next for loop
            # del response['meta']
            # del response['result_message']
            # del response['result_output']
            # we need to delete the column fields as too much unneccessary data is shown
            # for client in response:
            #     del response[str(client)]['fields']
            # try:
            #     df = df.append(pd.DataFrame.from_dict(response['ecomOrders']),ignore_index=True,sort=False)
            #     print(df)
            #     break
            # except TypeError:
            #     logger.error(str(e) + ' batch nr: ' + str(f) + ', element:' + str(response['ecomOrders']))
            #     continue
            finished_json_list.append(response['ecomOrders'])
    except Exception as e:
        logger.error(e) 
        logger.info('retry batch nr: ' + str(batch)) 
        finished, unfinished = await asyncio.wait(tasks, return_when=asyncio.ALL_COMPLETED)
        try:
            for task in finished:
                finished_json_list.append(task.result())
            finished = 0
        except Exception as e:
            logger.error(e + ' batch nr: ' + str(batch))
            pass
    with open(os.path.join(workingData, filename), 'wb') as f:
        pickle.dump(finished_json_list, f)
    return finished_json_list

#  main script can run until no more data is returned
async def main(batch_size = batchsize,batch_start = 1, batch =1):
    batch_end = batch_start + batch_size 
    # check how much data there is to collect
    logger.info('Assement of maximum id value')
    params = [("api_action", 'contact_list'), ("api_key", token), ("api_output", "json")]
    data = [('limit', 100), ('offset',0), ('orders[id]','ASC')] 
    params += data
    data = []
    df = pd.DataFrame()
    response = requests.request('GET',url,params=params).json()
    max_id = int(response['meta']['total'])
    logger.info('max_id: ' + str(max_id))
    current_id = 0
    while batch < 2:
        logger.info('batch nr: ' + str(batch) + ', batch_start: ' + str(batch_start) + ', batch_end: ' + str(batch_end))
        finished_json_list = 0
        finished_json_list = await get_batch(batch_start = batch_start,batch_end = batch_end, batch = batch)
        # now we try and see if the resulting list_ac_json contains anything
        # if we did not receive an answer than we try again with more time before timeout
        # if both attemps are unsuccessful we save it to dictionary not_successful
        try:
            for ac_json in finished_json_list:
                df = df.append(pd.DataFrame.from_dict(ac_json),ignore_index=True,sort=False)
        except Exception as e:
            logger.error(e)
        #     try:
        #         logger.info('retry batch nr: ' + str(batch) + ', batch_start: ' + str(batch_start) + ', batch_end: ' + str(batch_end))
        #         finished_json_list = await get_batch(batch_start = batch_start, batch_end = batch_end, batch = batch)       
        #         current_id = int(finished_json_list[(batch_size-1)]['19']['id'])
        #         logger.info('success batch nr: ' + str(batch) + ', batch_start: ' + str(batch_start) + ', batch_end: ' + str(batch_end))
        #     # as our code has failed twice lets see if this is the last batch, where we have the max_id we are looking for
        #     except Exception as e:
        #         logger.error(e)
        #         try:
        #             for ac_json in finished_json_list:
        #                 if ac_json['result_message'] == 'Success: Something is returned':
        #                     for contact in ac_json:
        #                         if contact in ['result_message','result_code','result_output']:
        #                             continue
        #                         else:
        #                             if int(ac_json[contact]['id']) == max_id:
        #                                 current_id = int(ac_json[contact]['id'])
        #                                 logger.info('ended successful, found max_id: ' + str(current_id) + ', in batch nr: ' + str(batch) + ', list_element: ' + str(contact))
        #                                 break     
                # except Exception as e:
                #     logger.error(e)
                #     logger.warning('ended unsuccessful current_id: ' + str(current_id) + ', max_id: ' + str(max_id))
                #     break
        batch += 1
        batch_start += batch_size
        batch_end += batch_size
        print(df)
        print(df['email'].value_counts())
        print(df[df.duplicated(['email','externalid'])])
        df.to_csv(os.path.join(workingData,'ecomOrders.csv'), sep='\t', encoding='utf-8')

 #  creates a loop required for asyncronious calls
if __name__ == '__main__':
    ts = time()
    #Create the asyncio event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(main())
    finally:
        # Shutdown the loop even if there is an exception
        loop.close()
    logger.info('Took %s seconds to complete', time() - ts)

# directory = os.path.join(workingData, 'ecom')
# def unpickle_multibatch(directory):
#     df = pd.DataFrame()
#     for f in listdir(directory):
#         filename, file_extension = os.path.splitext(f)
#         if isfile(join(directory, f)) and file_extension == ".pickle":
#             print(f)
#             with open(os.path.join(directory, f),"rb") as f2:
#                 df_new = pickle.load(f2)
#                 df = df.append(df_new,ignore_index=True,sort=False)
#     return df

# data = unpickle_multibatch(multibatch_directory)
# data
